// pages/add-place/add-place.js
const app=getApp();
Component({

  /**
   * 页面的初始数据
   */
  data: {
    address:'',
    name:''

  },
  options: {
    addGlobalClass: true,
  },
  ready: function () {
    this.showAddress()
  },

  methods: {
    save:function()
    {
      let data={
        name:this.name,
        address:this.address,
        longitude: app.globalData.lng,
          latitude:app.globalData.lat,
      }
      console.log("app.globalData.token", app.globalData.token)
      wx.request({
        url: app.globalData.domain +'/place/save?token='+app.globalData.token,
        data: data,
        header: {
          'content-type': 'application/json'
        },
        method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        success: function (res) {

          console.log("save:", res);
          wx.showToast({
            title: '已提交',
            icon: 'success',
            duration: 2000
          })
         // that.token = res.data.token;
        }
      })
        
    },
    watchplace:function(e)
    {
      this.name=e.detail.value
    },

    showAddress:function()
    {
      let that = this;
      that.address = app.globalData.address
      console.log("address:", app.globalData.address)
      this.setData({
        address: that.address
      })
    }
  }

})