// pages/home/home.js
const app = getApp();
Component({
  options: {
    addGlobalClass: true,
  },
  /**
   * 页面的初始数据
   */
  data: {
    showEditInfo:false,
    dialogModal1:false,
list:[],
total:0,
    pageNum:1,
    pageSize:10,
  },
  ready: function () {
    let that=this;

    if (app.globalData.lat)
    {
      that.getPageList();
    }
    else
    {
     let ti= setInterval(
      
        function () {
          if (app.globalData.lat) {
            clearInterval(ti);
            that.getPageList();
          
          }

        }
        
        , 3000);
    }
    if (app.globalData.token&&(!app.globalData.userInfo || !app.globalData.userInfo.nickName)) {
      that.data.showEditInfo = true;
      that.setData({
        showEditInfo: that.data.showEditInfo
      })

    }
   

 
    app.globalData.callbackLogin = function () {

      if (!app.globalData.userInfo || !app.globalData.userInfo.nickName)
      {
        that.data.showEditInfo = true;
      
      }
      else
      {
        that.data.showEditInfo = false;
      }

      that.setData({
        showEditInfo: that.data.showEditInfo
      })

    }
 
  },

  methods: {
    cancel:function(e)
    {
      this.data.dialogModal1=false;
      this.setData({
        dialogModal1: this.data.dialogModal1
      })
    },
    confirm: function (e) {
      this.data.dialogModal1 = false;
      this.setData({
        dialogModal1: this.data.dialogModal1
      })
      wx.navigateTo({
        url: '../edit-my-info/edit-my-info'
      })
    },
    placeJoin:function(placeId)
    {
      let that=this;
      wx.request({
        url: app.globalData.domain + '/placeJoin/save?token=' + app.globalData.token,
        data: {
          placeId:placeId
        },
        header: {
          'content-type': 'application/json'
        },
        method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        success: function (res) {

          app.globalData.placeId=placeId;
          that.setData({
            placeId: placeId  
          })

        }
      })
    },
    getPageList:function()
    {
      let that=this;
      wx.request({
        url: app.globalData.domain + '/place/list?token=' + app.globalData.token,
        data: {
          pageNum: this.data.pageNum,
          pageSize: this.data.pageSize,
          latitude: app.globalData.lat,
          longitude: app.globalData.lng,
        },
        header: {
          'content-type': 'application/json'
        },
        method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        success: function (res) {

          console.log("getPageList:", res);
        that.data.list=res.data.page.list;
          that.data.total = res.data.page.totalCount;

          if(!app.globalData.placeId)
          {
            that.placeJoin(that.data.list[0].id)
          }

          that.setData({
            list:that.data.list
          })

        }
      })
    },
    gotoChat: function (e) {

      if (!app.globalData.userInfo ||!app.globalData.userInfo.nickName)
      {
        this.data.dialogModal1 = true;
        this.setData({
          dialogModal1: this.data.dialogModal1
        })
        return;
      }

      var id = e.currentTarget.dataset.id;
      this.placeJoin(id)
      wx.navigateTo({
        url: '../place/index?placeId='+id
      })
    },
    gotoEditMyInfo: function (e) {
      wx.navigateTo({
        url: '../edit-my-info/edit-my-info'
      })
    },
  },


  // ListTouch触摸开始
  ListTouchStart(e) {
    this.setData({
      ListTouchStart: e.touches[0].pageX
    })
  },

  // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection == 'left') {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("home")
  },

 

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("home3")
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("home4")
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log("home3")
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})