const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    CustomBar: app.globalData.CustomBar,
    user:{
      
    },
    loadProgress: 0,
    isLoad:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo();
    this.loadProgress()
  },
  ChooseImage() {
    wx.chooseImage({
      count: 1, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        this.upload(res.tempFilePaths)
     
      }
    });
  },


  upload: function (filePath) {
    var that = this
  
      wx.uploadFile({
        url: app.globalData.domain + '/upload/upload?token=' + app.globalData.token,
        filePath: filePath[0],
        name: 'file',
        formData: {},
        success: function (res) {
          console.log(res)
          if (res) {
           let data= JSON.parse(res.data)
            that.data.user.avatar=data.msg;

            that.setData({
              avatar: that.data.user.avatar
            })
          }
        }
      })
    
    this.setData({
      formdata: ''
    })
  },

  changeSex:function(e)
  {
      console.log(e)
      if(e.detail.value)
      {
         this.data.user.sex=1
      }
      else
      {
        this.data.user.sex = 0
      }
  },
  DateChange(e) {
    this.data.user.birthday = e.detail.value
    this.setData({
      birthday: e.detail.value
    })
  },

  getInfo:function()
  {
    let that=this;
    wx.request({
      url: app.globalData.domain + '/user/info?token=' + app.globalData.token,
      data: {
       
      },
      header: {
        'content-type': 'application/json'
      },
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      success: function (res) {
        console.log("user",res);
        that.data.user=res.data.user;

        if (!that.data.user.birthday)
        {
          that.data.user.birthday='1996-12-29'
        }
        if (that.data.user.sex===null) {
          that.data.user.sex=1
        }

        let obj = that.data.user;

        console.log("obj",obj);
        that.setData(
          obj
        )

      }
    })

  },


  watchName:function(e)
  {
    this.data.user.nickName = e.detail.value
  },
  watchIntroduction:function(e)
  {
    this.data.user.introduction = e.detail.value
  },

  save: function () {
    let that=this;
    this.setData({
      isLoad:false
    })
    console.log("app.globalData.token", app.globalData.token)
    wx.request({
      url: app.globalData.domain + '/user/save?token=' + app.globalData.token,
      data: this.data.user,
      header: {
        'content-type': 'application/json'
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      success: function (res) {

        console.log("save:", res);
        // that.token = res.data.token;
        app.globalData.userInfo = that.data.user
        that.setData({
          isLoad: true
        })
        wx.showToast({
          title: '已提交',
          icon: 'success',
          duration: 2000
        })

        var pages = getCurrentPages(); //当前页面
        var beforePage = pages[pages.length - 2]; //前一页
        wx.navigateBack({
          success: function () {
            beforePage.onLoad(); // 执行前一个页面的onLoad方法
            beforePage.onReady(); // 执行前一个页面的onLoad方法
          }
        });
      }
    })

  },
  loadProgress() {
    this.setData({
      loadProgress: this.data.loadProgress + 3
    })
    if (this.data.loadProgress < 100) {
      setTimeout(() => {
        this.loadProgress();
      }, 100)
    } else {
      this.setData({
        loadProgress: 0
      })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})