const app = getApp();
Page({
  data: {
    myInfo:{},
 
    total: 0,
    pageNum: 1,
    pageSize: 10,
    chatList: [],
    CustomBar: app.globalData.CustomBar,
    InputBottom: 0,
    checkbox: [],
    content:'',
    id:0,
  },
  onLoad: function (options) {
    console.log(options)
this.data.id=options.id;
    this.setData({
      id: options.id,
      nickName: options.nickName,
      avatar: options.avatar

    })
   
  },
  getPageList: function () {
    let that = this;
    wx.request({
      url: app.globalData.domain + '/chat/list?token=' + app.globalData.token,
      data: {
        pageNum: this.data.pageNum,
        pageSize: this.data.pageSize,
        action: that.data.myInfo.id,
        receiver: that.data.id

      },
      header: {
        'content-type': 'application/json'
      },
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      success: function (res) {

        console.log("getPageList:", res);
     //   that.data.chatList = res.data.list;
        that.data.total = res.data.total;
        for (var i=0;i<res.data.list.length;i++)
        {
          that.data.chatList.push(res.data.list[i])
        }


        that.setData({
          chatList: that.data.chatList
        })
        app.globalData.callback = function (res) {

          console.log('接收到的消息resData', res)

          let data = {
            action: res.action,
            receiver: res.receiver,
            content: res.content,

          }

          that.data.chatList.push(data)
          that.setData({
            chatList: that.data.chatList
          })

        }
      }
    })
  },
  onShow: function () {
    let that=this

 
    this.load()
    this.getPageList();

  },
  load:function()
  {
    let that = this
  //  that.data.chatList = app.globalData.chatList;
    that.data.myInfo = app.globalData.userInfo
    console.log(" this.data.chatList", that.data.chatList)
    console.log("  that.data.myInfo", that.data.myInfo)
    this.setData({
      myInfo: that.data.myInfo
    })
  },
  btnSend(e){
    let data = {
      action: this.data.myInfo.id+"",
      receiver:this.data.id,
      content: this.data.content,

    }

    let that = this;
    wx.request({
      url: app.globalData.domain2 + '/api/message/send',
      data: data,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      success: function (res) {
        console.log("/api/message/send",res)
        if (!res.data)
        {
          return;
        }
        that.data.chatList.push(data)
        that.setData({
          chatList: that.data.chatList
        })
        that.data.content = '';
        that.setData({
          content: ''
        })

      }
    })
  
  },
  inputInput(e)
  {
    console.log('e2', e)
    this.data.content = e.detail.value
  },
  InputFocus(e) {

    console.log('e1', e)
    this.setData({
      InputBottom: e.detail.height
    })
  },
  InputBlur(e) {
    console.log('e',e)
    this.setData({
      InputBottom: 0
    })
  }
,
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  ChooseCheckbox(e) {
    let items = this.data.checkbox;
    let values = e.currentTarget.dataset.value;
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      if (items[i].value == values) {
        items[i].checked = !items[i].checked;
        break
      }
    }
    this.setData({
      checkbox: items
    })
  },
  gotoShop(e)
  {
    wx.navigateTo({
      url: '../shop/shop?type=chat'
    })
  }

})