// pages/message/message.js
const app = getApp();
Component({
  options: {
    addGlobalClass: true,
  },
  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    total: 0,
    pageNum: 1,
    pageSize: 10,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  ready: function () {
    this.getPageList();
  },
  methods: {
    gotoChat: function (e) {
      console.log("e", e)
      var item = e.currentTarget.dataset.item;
      wx.navigateTo({
        url: `../chat/chat?id=${item.userId}&nickName=${item.nickName}&avatar=${item.avatar}`
      })
    },
    getPageList: function () {
      let that = this;
      wx.request({
        url: app.globalData.domain + '/buddy/list?token=' + app.globalData.token,
        data: {
          pageNum: this.data.pageNum,
          pageSize: this.data.pageSize,
      
        },
        header: {
          'content-type': 'application/json'
        },
        method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        success: function (res) {

          console.log("getPageList:", res);
          that.data.list = res.data.list;
          that.data.total = res.data.total;

       
          that.setData({
            list: that.data.list
          })

        }
      })
    },
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})