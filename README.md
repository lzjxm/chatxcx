# 微信小程序即时通讯
## 个人怎么测试？在app.js登录接口传自己小程序的appid和secret
![输入图片说明](https://images.gitee.com/uploads/images/2020/0628/105307_5f195d8e_383785.png "微信截图_20200628105108.png") 
### 然后再修改开发工具的配置appid
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0628/114941_56859712_383785.png "微信截图_20200628114918.png")

## 实例

| ![输入图片说明](https://images.gitee.com/uploads/images/2020/0628/101216_e1413ef0_383785.png "微信截图_20200628100652.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0628/101228_b965a244_383785.png "微信截图_20200628100709.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0628/101239_35446a54_383785.png "微信截图_20200628100731.png")  |
|---|---|---|

 **后台框架来源：** 
[https://gitee.com/farsunset/cim?_from=gitee_search](https://gitee.com/farsunset/cim?_from=gitee_search)

 **后台地址**
 [https://chat.luozejiao.com/console/session/list](https://chat.luozejiao.com/console/session/list)



