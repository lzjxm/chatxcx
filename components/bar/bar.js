Page({

  data: {
    pageCur: 'home'
  },
  navChange(e) {
    this.triggerEvent('navChange', {
      pageCur: e.currentTarget.dataset.cur
    })
    this.setData({
      pageCur: e.currentTarget.dataset.cur
    })
  },
})