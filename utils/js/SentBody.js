module.exports = {

  "nested": {
    "com": {
      "nested": {
        "farsunset": {
          "nested": {
            "cim": {
              "nested": {
                "sdk": {
                  "nested": {
                    "web": {
                      "nested": {
                        "model": {
                          "nested": {
                            "SentBody": {
                              "fields": {
                                "key": {
                                  "type": "string",
                                  "id": 1
                                },
                                "timestamp": {
                                  "type": "int64",
                                  "id": 2
                                },
                                "data": {
                                  "keyType": "string",
                                  "type": "string",
                                  "id": 3
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}